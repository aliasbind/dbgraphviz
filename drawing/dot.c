#include <dot.h>
#include <utils.h>

#ifdef WIN32
#include <windows.h>
#endif

char *img_filepath;

/*
 * Creates a HTML table which describes the structure of the given SQL table
 * found at 'table_index' in the global 'tables' array.
 */
static char *build_table_section(int table_index)
{
	char *buf;
	int buf_idx = 0, i;
	struct table *table = &tables[table_index];

	buf = malloc(sizeof(char) * TABLE_SECTION_SIZE);

	/* Add HTML header for a Table */
	buf_idx = sprintf(&buf[buf_idx], "t%d [\n%s",
			table_index, DOT_TABLE_HEADER);

	/* Add table name as a row spanning two HTML Table columns */
	buf_idx += sprintf(&buf[buf_idx], DOT_TABLE_NAME_FMT, table->name);

	/*
	 * Add a row for each SQL column that contains two HTML columns, one
	 * with the column name and the other one with the column type.
	 *
	 * FIXME: Should detail the following operation in depth.
	 */
	for (i = 0; i < table->num_cols; i++)
		buf_idx += sprintf(&buf[buf_idx], DOT_FKEY_COL_FMT,
				i, table->cols[i].name, table->cols[i].type);

	sprintf(&buf[buf_idx], "</TABLE>>]\n");

	return buf;
}

/* Creates a fully compatible DOT file for the global 'tables' array */
static void build_dot_common(const char *tmpfile_in, const char *tmpfile_out)
{
	FILE *tmpfile;
	char *table_section;
	char command[512];

	int i;

	/*
	 * At this point, 'tmpfilepath' should have the format
	 *
	 * '/tmp/dbgraphviz-XXXXXX.dot'
	 *
	 * and 'outfilepath' should be
	 *
	 * '/tmp/dbgraphviz-XXXXXX.svg'
	 *
	 * where 'XXXXXX' is a random and unique combination of
	 * alphanumerical characters.
	 */

	tmpfile = fopen(&tmpfile_in[0], "w+");
	if (!tmpfile)
		SYS_FAIL(tmpfile_in);

	/* Write begining of the dot file */
	fprintf(tmpfile, DOT_HEADER);

	/* Create an entry for every table and write it to the file */
	for (i = 0; i < tables_len; i++) {
		table_section = build_table_section(i);

		fprintf(tmpfile, table_section);

		free(table_section);
	}

	/* Append the edges for every table */
	for (i = 0; i < links_len; i++)
		fprintf(tmpfile, "t%d:%d -> t%d:%d\n", links[i].table_src,
				links[i].col_src, links[i].table_dst,
				links[i].col_dst);

	/* End of the DOT file, add closing bracket. */
	fprintf(tmpfile, "}");
	fclose(tmpfile);

	/* Generate SVG using the 'dot' command. */
	strcpy(&command[0], "dot -Tsvg ");
	strcat(&command[0], tmpfile_in);
	strcat(&command[0], " -o ");
	strcat(&command[0], tmpfile_out);

	if (system(&command[0]))
		GEN_FAIL("Failed to execute \"%s\"\n", &command[0]);
	else
		INFO("Successfully executed %s\n", &command[0]);

	if (img_filepath)
		free(img_filepath);

	/* Store SVG filepath in a global variable */
	img_filepath = strdup(&tmpfile_out[0]);

	/* Remove DOT file */
	remove(&tmpfile_in[0]);
}

#ifdef WIN32

void build_dot(void)
{
	DWORD dwRetVal = 0;
	UINT uRetVal   = 0;

	TCHAR szTempFileNameIn[MAX_PATH];
	TCHAR szTempFileNameOut[MAX_PATH];
	TCHAR lpTempPathBuffer[MAX_PATH];

	//  Gets the temp path env string (no guarantee it's a valid path).
	dwRetVal = GetTempPath(MAX_PATH,          // length of the buffer
			lpTempPathBuffer); // buffer for path
	if (dwRetVal > MAX_PATH || (dwRetVal == 0))
		SYS_FAIL("GetTempPath");

	//  Generates a temporary file name.
	uRetVal = GetTempFileName(lpTempPathBuffer, // directory for tmp files
			TEXT("dgv"),     // temp file name prefix
			0,                // create unique name
			szTempFileNameIn);  // buffer for name

	if (uRetVal == 0)
		SYS_FAIL("GetTempFileName");

	strcpy(szTempFileNameOut, szTempFileNameIn);
	szTempFileNameOut[strlen(szTempFileNameOut) - 3] = '\0';
	strcat(szTempFileNameOut, "svg");

	build_dot_common(szTempFileNameIn, szTempFileNameOut);
}

#else

void build_dot(void)
{
	char tmpfilename[sizeof(TMP_FILENAME)];
	char tmpfilepath[256];
	char outfilepath[256];

	/* Set temporary file name and path */
	strcpy(&tmpfilepath[0], "/tmp/");
	strcpy(&tmpfilename[0], TMP_FILENAME);

	if (!mktemp(&tmpfilename[0]))
		SYS_FAIL("mktemp");

	strcpy(&outfilepath[0], &tmpfilepath[0]);
	strcat(&outfilepath[0], &tmpfilename[0]);
	strcat(&outfilepath[0], ".svg");

	strcat(&tmpfilepath[0], &tmpfilename[0]);
	strcat(&tmpfilepath[0], ".dot");

	build_dot_common(&tmpfilepath[0], &outfilepath[0]);
}

#endif
