#include <drawing.h>

SDL_Surface *main_screen;
cairo_surface_t *cairo_main_screen;

cairo_t *cairo_svg_handle;
RsvgHandle *rsvg_svg_handle;

int screen_w;
int screen_h;

void video_init(void)
{
	const SDL_VideoInfo *info;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		SDL_FAIL("SDL_Init");

	SDL_EnableKeyRepeat(250, 35);

	INFO("SDL initialized\n");

	info = SDL_GetVideoInfo();
	INFO("Screen has a %d x %d resolution.\n",
			info->current_w, info->current_h);

	screen_w = WINDOW_RATIO * info->current_w;
	screen_h = WINDOW_RATIO * info->current_h;

	INFO("Creating a screen with a %d x %d resolution.\n",
			screen_w, screen_h);

	main_screen = SDL_SetVideoMode(screen_w, screen_h, 32, SCREEN_FLAGS);
	cairo_main_screen = cairo_image_surface_create_for_data(
			main_screen->pixels,
			CAIRO_FORMAT_RGB24,
			main_screen->w,
			main_screen->h,
			main_screen->pitch);

	g_type_init();
}

void print_event_type(SDL_EventType type)
{
	printf("EV: ");

	switch (type) {
	case SDL_NOEVENT:
		printf("SDL_NOEVENT\n");
		break;
	case SDL_ACTIVEEVENT:
		printf("SDL_ACTIVEEVENT\n");
		break;
	case SDL_KEYDOWN:
		printf("SDL_KEYDOWN\n");
		break;
	case SDL_KEYUP:
		printf("SDL_KEYUP\n");
		break;
	case SDL_MOUSEMOTION:
		printf("SDL_MOUSEMOTION\n");
		break;
	case SDL_MOUSEBUTTONDOWN:
		printf("SDL_MOUSEBUTTONDOWN\n");
		break;
	case SDL_MOUSEBUTTONUP:
		printf("SDL_MOUSEBUTTONUP\n");
		break;
	case SDL_JOYAXISMOTION:
		printf("SDL_JOYAXISMOTION\n");
		break;
	case SDL_JOYBALLMOTION:
		printf("SDL_JOYBALLMOTION\n");
		break;
	case SDL_JOYHATMOTION:
		printf("SDL_JOYHATMOTION\n");
		break;
	case SDL_JOYBUTTONDOWN:
		printf("SDL_JOYBUTTONDOWN\n");
		break;
	case SDL_JOYBUTTONUP:
		printf("SDL_JOYBUTTONUP\n");
		break;
	case SDL_QUIT:
		printf("SDL_QUIT\n");
		break;
	case SDL_SYSWMEVENT:
		printf("SDL_SYSWMEVENT\n");
		break;
	case SDL_EVENT_RESERVEDA:
		printf("SDL_EVENT_RESERVEDA\n");
		break;
	case SDL_EVENT_RESERVEDB:
		printf("SDL_EVENT_RESERVEDB\n");
		break;
	case SDL_VIDEORESIZE:
		printf("SDL_VIDEORESIZE\n");
		break;
	case SDL_VIDEOEXPOSE:
		printf("SDL_VIDEOEXPOSE\n");
		break;
	case SDL_EVENT_RESERVED2:
		printf("SDL_EVENT_RESERVED2\n");
		break;
	case SDL_EVENT_RESERVED3:
		printf("SDL_EVENT_RESERVED3\n");
		break;
	case SDL_EVENT_RESERVED4:
		printf("SDL_EVENT_RESERVED4\n");
		break;
	case SDL_EVENT_RESERVED5:
		printf("SDL_EVENT_RESERVED5\n");
		break;
	case SDL_EVENT_RESERVED6:
		printf("SDL_EVENT_RESERVED6\n");
		break;
	case SDL_EVENT_RESERVED7:
		printf("SDL_EVENT_RESERVED7\n");
		break;
	}
}

static void clear_screen(void)
{
	SDL_FillRect(main_screen, NULL, 0xFFFFFF);
}

void draw_dot(void)
{
	if (!img_filepath)
		return;

	rsvg_svg_handle = rsvg_handle_new_from_file(&img_filepath[0], NULL);
	if (!rsvg_svg_handle) {
		WARN("img_handle (%s)\n", &img_filepath[0]);
		return;
	}

	cairo_svg_handle = cairo_create(cairo_main_screen);
	if (!cairo_svg_handle) {
		WARN("cairo_handle\n");
		return;
	}
}

void repaint(double zoom, double trans_x, double trans_y)
{
	static double zoom_level = 1;
	static double offset_x;
	static double offset_y;

	double view_width = screen_w;
	double view_height = screen_h;

	double zoom_offset_x = 0;
	double zoom_offset_y = 0;

	clear_screen();

	zoom_level *= zoom;
	if (zoom_level > 8)
		return;

	if (!cairo_svg_handle)
		return;

	cairo_identity_matrix(cairo_svg_handle);
	cairo_scale(cairo_svg_handle, zoom_level, zoom_level);

	cairo_device_to_user(cairo_svg_handle, &trans_x, &trans_y);
	offset_x += trans_x;
	offset_y += trans_y;

	cairo_device_to_user(cairo_svg_handle, &view_width, &view_height);
	zoom_offset_x = (view_width - screen_w) / 2;
	zoom_offset_y = (view_height - screen_h) / 2;

	cairo_translate(cairo_svg_handle, offset_x + zoom_offset_x,
				offset_y + zoom_offset_y);

	if (!rsvg_handle_render_cairo(rsvg_svg_handle, cairo_svg_handle))
		WARN("Drawing failed!\n");

	SDL_Flip(main_screen);
}

void event_loop(void)
{
	SDL_Event event;

	while (SDL_WaitEvent(&event)) {
		switch (event.type) {
		case SDL_MOUSEBUTTONDOWN:
			mouse_handler(&event.button);
			break;
		case SDL_MOUSEMOTION:
			motion_handler(&event.motion);
			break;
		case SDL_KEYDOWN:
			key_handler(&event.key);
			break;
		case SDL_QUIT:
			quit_handler();
			break;
		default:
			break;
		}
	}
}
