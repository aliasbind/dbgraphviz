#include "handler.h"

static int mouse_start_x, mouse_start_y;

void key_handler(SDL_KeyboardEvent *event)
{
	INFO("Handling keydown\n");

	switch (event->keysym.sym) {
	case SDLK_q:
		quit_handler();
		break;
	default:
		break;
	}
}

void mouse_handler(SDL_MouseButtonEvent *event)
{
	SDL_EventState(SDL_MOUSEBUTTONDOWN, SDL_IGNORE);

	switch (event->button) {
	case SDL_BUTTON_WHEELUP:
		repaint(1 + ZOOM_ACCEL, 0, 0);
		break;
	case SDL_BUTTON_WHEELDOWN:
		repaint(1 / (1 + ZOOM_ACCEL), 0, 0);
		break;
	default:
		mouse_start_x = event->x;
		mouse_start_y = event->y;
		break;
	}

	SDL_EventState(SDL_MOUSEBUTTONDOWN, SDL_ENABLE);
}

void motion_handler(SDL_MouseMotionEvent *event)
{
	static double mouse_dx, mouse_dy;

	/* Handle only left clicks, for now */
	if (!(event->state & SDL_BUTTON_LEFT))
		return;

	SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);

	/*
	 * Store difference between previous mouse position and current
	 * position. These variables will be later used to translate the
	 * surface.
	 */
	mouse_dx = event->x - mouse_start_x;
	mouse_dy = event->y - mouse_start_y;

	repaint(1, mouse_dx, mouse_dy);

	mouse_start_x = event->x;
	mouse_start_y = event->y;

	SDL_EventState(SDL_MOUSEMOTION, SDL_ENABLE);
}

void quit_handler(void)
{
	INFO("Exiting\n");

	SDL_Quit();
	remove(img_filepath);
	free_tables();
	exit(EXIT_SUCCESS);
}
