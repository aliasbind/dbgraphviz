@echo off

SET CURRDIR=%~dp0
SET PATH=%PATH%;%CURRDIR%\dll;%CURRDIR%\graphviz\bin
SET PATH=%PATH%;%CURRDIR%\codeblocks-project\bin\Release
SET PATH=%PATH%;bin

dbgraphviz.exe -u root -P 9999 %*

pause
