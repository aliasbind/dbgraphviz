@echo off

md dbgraphviz-demo

md dbgraphviz-demo\dbbuild
copy ..\sample\sakila-schema.sql dbgraphviz-demo\dbbuild
copy ..\sample\sakila-data.sql dbgraphviz-demo\dbbuild

copy "1. mysql-server-start.bat" dbgraphviz-demo
copy "2. update-database.bat" dbgraphviz-demo
copy "3. dbgraphviz-client.bat" dbgraphviz-demo
copy "4. mysql-server-stop.bat" dbgraphviz-demo
copy utils\vcredist_x86.exe dbgraphviz-demo

md dbgraphviz-demo\dll
copy dll dbgraphviz-demo\dll

md dbgraphviz-demo\graphviz
xcopy /S /E /I /H graphviz dbgraphviz-demo\graphviz

md dbgraphviz-demo\mysql
xcopy /S /E /I /H mysql dbgraphviz-demo\mysql

md dbgraphviz-demo\bin
copy codeblocks-project\bin\Release\dbgraphviz.exe dbgraphviz-demo\bin

utils\7z.exe a -r -tzip dbgraphviz-demo.zip dbgraphviz-demo
rmdir /S /Q dbgraphviz-demo

pause
