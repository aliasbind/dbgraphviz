@echo off

SET CURRDIR=%~dp0
SET PATH=%PATH%;%CURRDIR%\mysql\bin

IF EXIST dbbuild\sakila-schema.sql (
	mysql.exe -u root -P 9999 < dbbuild\sakila-schema.sql
	mysql.exe -u root -P 9999 < dbbuild\sakila-data.sql
) ELSE (
	mysql.exe -u root -P 9999 < ..\sample\sakila-schema.sql
	mysql.exe -u root -P 9999 < ..\sample\sakila-data.sql
)

pause
