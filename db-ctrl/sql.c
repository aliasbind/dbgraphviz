#include <sql.h>

struct table *tables;
int tables_len;

struct table_link *links;
int links_len;

struct table_col create_col(const char *col_name, const char *col_type)
{
	struct table_col col;

	col.name = strdup(col_name);
	if (!col.name)
		SYS_FAIL("strdup");
	col.type = strdup(col_type);
	if (!col.type)
		SYS_FAIL("strdup");

	return col;
}

void free_col(struct table_col *col)
{
	free(col->name);
	free(col->type);
}

void table_add_col(struct table *table, struct table_col *col)
{
	/* Used to expand array size, if needed. */
	int old_size, new_size;
	int num_cols = table->num_cols;

	old_size = (num_cols / TABLE_ARRAY_SEGMENT_SIZE + 1) *
		TABLE_ARRAY_SEGMENT_SIZE;

	new_size = ((num_cols + 1) / TABLE_ARRAY_SEGMENT_SIZE + 1) *
		TABLE_ARRAY_SEGMENT_SIZE;

	if (num_cols == 0)
		table->cols = malloc(sizeof(struct table_col) *
				TABLE_ARRAY_SEGMENT_SIZE);
	else if (old_size != new_size)
		table->cols = realloc(&table->cols[0],
				sizeof(struct table_col) * new_size);

	if (!table->cols)
		SYS_FAIL("malloc");

	table->cols[num_cols++] = *col;
	table->num_cols = num_cols;
}

void init_table(struct table *table)
{
	table->name = NULL;
	table->cols = NULL;
	table->num_cols = 0;
}

void free_table(struct table *table)
{
	int i;

	for (i = 0; i < table->num_cols; i++)
		free_col(&table->cols[i]);

	free(table->name);
	free(table->cols);
}

void free_tables(void)
{
	int i;

	for (i = 0; i < tables_len; i++)
		free_table(&tables[i]);
	free(tables);

	tables = NULL;
	tables_len = 0;

	free(links);
	links_len = 0;
}

static void get_columns(struct table *table, MYSQL_RES *res)
{
	MYSQL_ROW row;
	struct table_col col;
	char col_type[256];

	while (row = mysql_fetch_row(res)) {
		/* Simplify table types */
		sscanf(row[1], "%[^\(]", &col_type[0]);
		col = create_col(row[0], &col_type[0]);
		table_add_col(table, &col);
	}
}

static void add_link(int table_src, int col_src, int table_dst, int col_dst)
{
	/* Used to expand array size, if needed. */
	int old_size, new_size;
	struct table_link link;

	old_size = (links_len / LINK_ARRAY_SEGMENT_SIZE + 1) *
		LINK_ARRAY_SEGMENT_SIZE;

	new_size = ((links_len + 1) / LINK_ARRAY_SEGMENT_SIZE + 1) *
		LINK_ARRAY_SEGMENT_SIZE;

	if (links_len == 0)
		links = malloc(sizeof(struct table_link) *
				LINK_ARRAY_SEGMENT_SIZE);
	else if (old_size != new_size)
		links = realloc(&links[0],
				sizeof(struct table_link) * new_size);

	if (!links)
		SYS_FAIL("malloc");

	link.table_src = table_src;
	link.col_src = col_src;
	link.table_dst = table_dst;
	link.col_dst = col_dst;

	links[links_len++] = link;
}

static void get_foreign_keys(struct table *table, MYSQL_RES *res)
{
	MYSQL_ROW row;

	int i;
	int table_src, col_src, table_dst, col_dst;

	while (row = mysql_fetch_row(res)) {
		/* Find source table index for foreign key */
		for (i = 0; i < tables_len; i++)
			if (!strcmp(tables[i].name, table->name)) {
				table_src = i;
				break;
			}

		/* Find column index from source table for foreign key */
		for (i = 0; i < table->num_cols; i++)
			if (!strcmp(table->cols[i].name, row[4])) {
				col_src = i;
				break;
			}

		/* Find destination table index for foreign key */
		for (i = 0; i < tables_len; i++)
			if (!strcmp(tables[i].name, row[0])) {
				table_dst = i;
				break;
			}

		/* Find column index from destination table for foreign key */
		for (i = 0; i < tables[table_dst].num_cols; i++)
			if (!strcmp(tables[table_dst].cols[i].name, row[1])) {
				col_dst = i;
				break;
			}

		add_link(table_src, col_src, table_dst, col_dst);
	}
}

static void print_links(void)
{
	int i;

	for (i = 0; i < links_len; i++)
		printf("%d %d %d %d\n", links[i].table_src, links[i].col_src,
				links[i].table_dst, links[i].col_dst);
}

static void dump_tables(void)
{
#ifndef WIN32
	int i, j;
	FILE *f;

	f = fopen("/tmp/tables.dump", "w+");

	for (i = 0; i < tables_len; i++) {
		fprintf(f, "%d: %s\n", i, tables[i].name);
		for (j = 0; j < tables[i].num_cols; j++)
			fprintf(f, "\t%d: %s\n", j, tables[i].cols[j].name);
	}

	fclose(f);
#endif
}

void process_tables(const char *host, const char *user, const char *db,
		unsigned int port)
{
	MYSQL *conn;
	MYSQL_RES *res, *table_res;
	MYSQL_ROW row;
	char *table_query;
	int i, num_rows;

	table_query = malloc(sizeof(char) * 256);
	if (!table_query)
		SYS_FAIL("malloc");

	conn = mysql_init(NULL);
	if (!conn) {
		free(table_query);
		SQL_FAIL("Failed to initialize connection.");
	}

	conn = mysql_real_connect(conn, host, user, NULL, db, port, NULL, 0);
	if (!conn) {
		free(table_query);
		SQL_FAIL("Failed to initialize connection.");
	}

	if (mysql_query(conn, "show full tables")) {
		free(table_query);
		SQL_FAIL("Failed query \"show tables;\".");
	}

	res = mysql_store_result(conn);
	num_rows = mysql_num_rows(res);

	if (tables)
		free_tables();
	tables = malloc(sizeof(struct table) * num_rows);
	if (!tables)
		SYS_FAIL("malloc");

	while (row = mysql_fetch_row(res)) {
		/* This is a view. Skip it. */
		if (strcmp(row[1], "VIEW") == 0)
			continue;

		init_table(&tables[tables_len]);
		tables[tables_len].name = strdup(row[0]);

		strcpy(&table_query[0], "desc ");
		strcat(&table_query[0], &tables[tables_len].name[0]);

		if (mysql_query(conn, &table_query[0])) {
			WARN("Failed query \"%s\".\n", &table_query[0]);
			free(tables[tables_len].name);
			tables[tables_len].name = NULL;
			continue;
		}

		table_res = mysql_store_result(conn);
		get_columns(&tables[tables_len], table_res);
		mysql_free_result(table_res);

		tables_len++;
	}

	mysql_free_result(res);
	mysql_close(conn);

	conn = mysql_init(NULL);
	if (!conn) {
		free(table_query);
		SQL_FAIL("Failed to initialize connection.");
	}

	conn = mysql_real_connect(conn, host, user, NULL, "INFORMATION_SCHEMA",
			port, NULL, 0);

	if (!conn) {
		free(table_query);
		SQL_FAIL("Failed to initialize connection.");
	}

	for (i = 0; i < tables_len; i++) {
		strcpy(&table_query[0], MYSQL_FKEY_QUERY);
		strcat(&table_query[0], "'");
		strcat(&table_query[0], &tables[i].name[0]);
		strcat(&table_query[0], "'");

		if (mysql_query(conn, &table_query[0])) {
			WARN("Failed query \"%s\".\n", &table_query[0]);
			continue;
		}

		table_res = mysql_store_result(conn);
		get_foreign_keys(&tables[i], table_res);
		mysql_free_result(table_res);
	}

	print_links();
	dump_tables();

	mysql_close(conn);
	free(table_query);
}
