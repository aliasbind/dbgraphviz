#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql.h>
#include <graphviz/xdot.h>

#include <utils.h>

int main(int argc, char *argv[])
{
	FILE *f;
	char *buf;
	size_t sz;
	xdot *x;

	f = fopen("noname.dot", "r");
	buf = malloc(4096 * sizeof(char));

	fseek(f, 0L, SEEK_END);
	sz = ftell(f);
	fseek(f, 0L, SEEK_SET);
	fread(&buf[0], sizeof(char), sz, f);

	INFO("%s\n", buf);
	x = parseXDot(&buf[0]);
	if (!x)
		GEN_FAIL("Miserable failure\n");

	fclose(f);
	free(buf);
	return EXIT_SUCCESS;
}
