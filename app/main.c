#include <stdlib.h>

#include "drawing.h"
#include "dot.h"
#include "sql.h"

static void print_usage(void)
{
	printf("Usage: dbgraphviz\n"
			"\t[-h hostname]\n"
			"\t[-u user]\n"
			"\t[-P port]\n"
			"\t[-p password]\n");
}

#ifdef WIN32
static void parse_args(int argc, wchar_t *argv[])
{
	int i;

	wchar_t *hostname = NULL, *user = NULL, *password = NULL;
	char _hostname[128], _user[32], _password[32];
	char *_hostnameptr = NULL, *_userptr = NULL, *_passwordptr = NULL;
	unsigned int port = 0;

	for (i = 1; i < argc; i++) {
		if (wcscmp(argv[i], L"-h") == 0) {
			if (i+1 >= argc || argv[i+1][0] == '-') {
				print_usage();
				continue;
			}
			hostname = argv[i+1];
		}
		if (wcscmp(argv[i], L"-u") == 0) {
			if (i+1 >= argc || argv[i+1][0] == '-') {
				print_usage();
				continue;
			}
			user = argv[i+1];
		}
		if (wcscmp(argv[i], L"-P") == 0) {
			if (i+1 >= argc || argv[i+1][0] == '-') {
				print_usage();
				continue;
			}
			port = _wtoi(argv[i+1]);
		}
	}

	if (!hostname)
		_hostname[0] = '\0';
	else {
		wcstombs(&_hostname[0], hostname, sizeof(_hostname));
		_hostnameptr = &_hostname[0];
	}
	if (!user)
		_user[0] = '\0';
	else {
		wcstombs(&_user[0], user, sizeof(_user));
		_userptr = &_user[0];
	}

	if (!password)
		_password[0] = '\0';
	else {
		wcstombs(&_password[0], password, sizeof(_password));
		_passwordptr = &_password[0];
	}

	if (!_hostnameptr)
		printf("hostnameptr is NULL\n");

	process_tables(_hostnameptr, _userptr, "sakila", port);
	build_dot();
	draw_dot();
	repaint(1, 0, 0);
}

#else
static void parse_args(int argc, char *argv[])
{
	int i;

	char *hostname = NULL, *user = NULL;
	unsigned int port = 0;

	printf("ARGC: %d\n", argc);
	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			if (i+1 >= argc || argv[i+1][0] == '-') {
				print_usage();
				continue;
			}
			hostname = argv[i+1];
		}
		if (strcmp(argv[i], "-u") == 0) {
			if (i+1 >= argc || argv[i+1][0] == '-') {
				print_usage();
				continue;
			}
			user = argv[i+1];
		}
		if (strcmp(argv[i], "-P") == 0) {
			if (i+1 >= argc || argv[i+1][0] == '-') {
				print_usage();
				continue;
			}
			port = atoi(argv[i+1]);
		}
	}

	process_tables(hostname, user, "sakila", port);
	build_dot();
	draw_dot();
	repaint(1, 0, 0);
}
#endif

#ifdef WIN32
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
		LPSTR lpCmdLine, int nCmdShow)
#else
int main(int argc, char *argv[])
#endif
{
#ifdef WIN32
	int argc;
	wchar_t **argv;

	argv = CommandLineToArgvW(GetCommandLineW(), &argc);
#endif
	video_init();

	parse_args(argc, argv);

	event_loop();

	return EXIT_SUCCESS;
}
