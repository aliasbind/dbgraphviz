#ifndef _DOT_H_
#define _DOT_H_

#include <sql.h>

#define TABLE_SECTION_SIZE	4096
#define DOT_FILE_SIZE		65536

#define TMP_FILENAME		"dbgraphviz-XXXXXX"

#define DOT_HEADER							\
"digraph G {\n"								\
	"graph [\n"							\
		"rankdir = RL\n"					\
	"]\n"								\
									\
	"node [\n"							\
		"shape = box\n"						\
		"style = \"rounded,filled\"\n"				\
		"penwidth = \"4\"\n"					\
		"color = \"#233138\"\n"					\
		"fillcolor = \"#3b5998\"\n"				\
									\
		"fontname = \"Monospace\"\n"				\
		"fontsize = \"9\"\n"					\
		"fontcolor = \"#fff7f5\""				\
	"]\n"								\
									\
	"edge [\n"							\
		"arrowsize = 0.7\n"					\
		"color = \"#40d040\"\n"					\
	"]\n"								\

#define DOT_TABLE_HEADER						\
"label=<<TABLE BGCOLOR=\"#3b5998\" BORDER=\"0\" "			\
"CELLSPACING=\"0\" CELLPADDING=\"3\">"					\

/* Parameters: (TABLE_NAME) */
#define DOT_TABLE_NAME_FMT						\
	"<TR><TD COLSPAN=\"2\" CELLPADDING=\"8\">%s</TD></TR>"		\

/* Parameters: (COLUMN_NAME, COLUMN_TYPE) */
#define DOT_REG_COL_FMT							\
	"<TR>"								\
		"<TD ALIGN=\"LEFT\">%s</TD>"				\
		"<TD ALIGN=\"RIGHT\">"					\
			"<FONT COLOR=\"#ff9f67\">%s</FONT>"		\
		"</TD>"							\
	"</TR>"								\

/* Parameters: (PORT_NUMBER, COLUMN_NAME, COLUMN_TYPE) */
#define DOT_FKEY_COL_FMT						\
	"<TR><TD ALIGN=\"LEFT\" COLSPAN=\"2\" PORT=\"%d\">"		\
	"<TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"0\">"	\
	"<TR>"								\
		"<TD ALIGN=\"LEFT\">%s</TD>"				\
		"<TD ALIGN=\"RIGHT\">"					\
			"<FONT COLOR=\"#ff9f67\">%s</FONT>"		\
		"</TD>"							\
	"</TR>"								\
	"</TABLE></TD></TR>"						\

extern char *img_filepath;

void build_dot(void);

#endif
