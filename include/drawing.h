#ifndef _DRAWING_H_
#define _DRAWING_H_

#include <utils.h>
#include <sql.h>
#include <dot.h>

#define RGB(c)		SDL_MapRGB(main_screen->format, c)

#define THEME_DEFAULT

#if defined(THEME_DEFAULT)

#define CELL_BG		0x3B, 0x59, 0x98
#define CELL_BORDER	0x23, 0x31, 0x38
#define CELL_TEXT	0xff, 0xf7, 0xf5
#define CELL_COLL_TYPE	0xff, 0x9f, 0x67

#elif defined(THEME_WOMBAT)

/* Does not work as expected */
#define CELL_BG		0x1c, 0x1c, 0x1c
#define CELL_BORDER	0x08, 0x08, 0x08
#define CELL_TEXT	0xdd, 0xdd, 0xdd
#define CELL_COLL_TYPE	0x54, 0xe4, 0x95

#endif

#define RED		0xff, 0x00, 0x00
#define GREEN		0x00, 0xff, 0x00
#define BLUE		0x00, 0x00, 0xff

#define WINDOW_RATIO	0.6f
#define SCREEN_FLAGS	(SDL_HWACCEL | SDL_HWSURFACE | SDL_DOUBLEBUF)

#define ZOOM_ACCEL	0.20

extern SDL_Surface *main_screen;

/* Current screen dimensions */
extern int screen_w;
extern int screen_h;

void video_init(void);

void event_loop(void);

void draw_dot(void);

/* Repaints the main surface
 *
 * zoom:
 *	Scales the surface.
 *
 *	If zoom is 1, the surface will not change.
 *	If zoom is <1, the surface will be zoomed in.
 *	If zoom is >1, the surface will be zoomed out.
 *
 * trans_x, trans_y:
 *	Translate the surface by 'trans_x' pixels to the left
 *	or right and 'trans_y' pixels up or down, depending on the
 *	parameter sign.
 *
 *	If trans_x and trans_y are 0, nothing will change.
 *
 */
void repaint(double zoom, double trans_x, double trans_y);

extern cairo_t *cairo_svg_handle;

#endif
