#ifndef _SQL_H_
#define _SQL_H_

#ifdef WIN32
#include <winsock.h>
#endif

#include <mysql.h>

#include <utils.h>

#define TABLE_NAME_MAX_LENGTH		64
#define TABLE_ARRAY_SEGMENT_SIZE	64
#define LINK_ARRAY_SEGMENT_SIZE		64

#define MYSQL_FKEY_QUERY	"select TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME from KEY_COLUMN_USAGE where REFERENCED_TABLE_NAME = "

/* Contains information about a table. */
struct table {
	/* The table name. */
	char *name;

	/* Array of all columns in a table. */
	struct table_col *cols;

	/* Number of columns. */
	int num_cols;
};

struct table_col {
	char *name;
	char *type;
};

struct table_link {
	int table_src;
	int col_src;
	int table_dst;
	int col_dst;
};

/*
 * Global pointer to all the tables, with their relationships found on the
 * given database. Initialized after calling process_tables().
 */
extern struct table *tables;
extern int tables_len;

extern struct table_link *links;
extern int links_len;

/*
 * Finds all the tables and their relationships in the database and stores
 * them in the 'tables' global array.
 */
void process_tables(const char *host, const char *user, const char *db,
		unsigned int port);

#endif
