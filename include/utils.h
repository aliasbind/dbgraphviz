#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include <cairo.h>
#include <SDL.h>
#include <librsvg/rsvg.h>

enum msg_types {
	SDL_ERROR,
	SYS_ERROR,
	SQL_ERROR,
	GEN_ERROR,
	OTHER,
};

enum error_level {
	WARNING,
	CRITICAL,
	INFO,
};

static void fail(int error_type, int error_level, const char *file, int line,
		const char *msg, ...)
{
	va_list args;

	fprintf(stderr, "%s:%d\n", file, line);

#ifndef WIN32
	switch (error_level) {
	case WARNING:
		fprintf(stderr, "\e[1;33m");
		break;
	case CRITICAL:
		fprintf(stderr, "\e[1;31m");
		break;
	case INFO:
		fprintf(stderr, "\e[1;37m");
		break;
	}
#endif

	switch (error_type) {
	case SDL_ERROR:
		fprintf(stderr, "%s: %s\n", msg, SDL_GetError());
		break;
	case SYS_ERROR:
		perror(msg);
		break;
	case SQL_ERROR:
		fprintf(stderr, "%s\n", msg);
		break;
	case GEN_ERROR:
	case OTHER:
		va_start(args, msg);
		vfprintf(stderr, msg, args);
		va_end(args);
		break;
	}

#ifndef WIN32
	fprintf(stderr, "\e[0m");
#endif

	/* Exit in case of critical failure */
	if (error_level == CRITICAL)
		exit(EXIT_FAILURE);
}

#define FAIL(type, level, msg, ...) \
	fail(type, level, __FILE__,  __LINE__, msg, ##__VA_ARGS__)

#define SDL_FAIL(msg)		FAIL(SDL_ERROR, CRITICAL, msg)
#define SYS_FAIL(msg)		FAIL(SYS_ERROR, CRITICAL, msg)
#define SQL_FAIL(msg)		FAIL(SQL_ERROR, CRITICAL, msg)
#define GEN_FAIL(msg, ...)	FAIL(GEN_ERROR, CRITICAL, msg, ##__VA_ARGS__)
#define WARN(msg, ...)		FAIL(OTHER, WARNING, msg, ##__VA_ARGS__)
#define INFO(msg, ...)		FAIL(OTHER, INFO, msg, ##__VA_ARGS__)

#endif
