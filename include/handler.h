#ifndef _HANDLER_H_
#define _HANDLER_H_

#include <drawing.h>
#include <dot.h>

void key_handler(SDL_KeyboardEvent *event);

void mouse_handler(SDL_MouseButtonEvent *event);
void motion_handler(SDL_MouseMotionEvent *event);

void quit_handler(void);

void handler_init(void);

#endif
